CONTENTS OF THIS FILE
---------------------

 * Introduction


INTRODUCTION
------------

Social Auth Drupal Simple Oauth Module is a Drupal Authentication integration for Drupal.


REQUIREMENTS
------------

This module requires the following modules:

 * Social Auth (https://drupal.org/project/social_auth)
 * Social API (https://drupal.org/project/social_api)


INSTALLATION
------------

 * Run composer to install dependencies:
   composer require "drupal/social_auth_drupal_simple_oauth"


HOOK
-------------
```
 function mymodule_social_auth_simple_oauth($data, $sdk) {
  $userId = $data->getId();
  $httpClient = \Drupal::httpClient();
  $config = \Drupal::configFactory()->get('social_auth_drupal_simple_oauth.settings');

  // server need enable restui module.
  // http://myserver.com/user/1
  $userInfo = $httpClient
    ->get($config->get('base_url') . '/user/' . $userId, [
      'query' => [
        '_format'      => 'json',
        'access_token' => $sdk->getAccessToken()
      ]
    ])
    ->getBody()->getContents();

  $userInfo = \GuzzleHttp\json_decode($userInfo, TRUE);

  $userInfo = array_map(function ($info) {
    if (count($info) == 1 && isset($info[0]['value'])) {
      return $info[0]['value'];
    }
  }, $userInfo);

  if (!isset($userInfo['mail'])) {
    exit('can\t get mail field');
  }

  if ($this->config->get('autocreate_user')) {
    $user = autoCreateUser($userInfo);
  }
  else {
    $user = user_load_by_mail($userInfo['mail']);
  }

  if (!$user) {
    exit('user not found');
  }

  user_login_finalize($user);
  $response = new RedirectResponse(Url::fromRoute('user.page')->toString());
  $response->send();
  exit();
}

function autoCreateUser($userInfo) {
  $mail = $userInfo['mail'];
  $name = $userInfo['name'];
  $timezone = $userInfo['timezone'];

  $user = user_load_by_mail($mail);

  if (!$user) {
    if (user_load_by_name($name)) {
      $name = explode('@', $mail)[0] . $name;
    }
    $user = User::create([
      'name' => $name,
      'mail' => $mail,
      'status' => 1,
      'timezone' => $timezone,
    ])->save();
  }
  return $user;
}
```

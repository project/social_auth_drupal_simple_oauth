<?php

namespace Drupal\social_auth_drupal_simple_oauth\Controller;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\Controller\OAuth2ControllerBase;
use Drupal\social_auth\SocialAuthDataHandler;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\social_auth_drupal_simple_oauth\DrupalAuthManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for Simple Drupal Connect module routes.
 */
class DrupalSimpleOauthAuthController extends OAuth2ControllerBase {

  /**
   * DrupalAuthController constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface      $messenger
   * @param \Drupal\social_api\Plugin\NetworkManager       $network_manager
   * @param \Drupal\social_auth\User\UserAuthenticator     $user_authenticator
   * @param \Drupal\social_auth_drupal_simple_oauth\DrupalAuthManager   $drupal_manager
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   * @param \Drupal\social_auth\SocialAuthDataHandler      $data_handler
   * @param \Drupal\Core\Render\RendererInterface          $renderer
   */
  public function __construct(MessengerInterface $messenger,
                              NetworkManager $network_manager,
                              UserAuthenticator $user_authenticator,
                              DrupalAuthManager $drupal_manager,
                              RequestStack $request,
                              SocialAuthDataHandler $data_handler,
                              RendererInterface $renderer) {


    parent::__construct('Social Auth Drupal Simple Oauth', 'social_auth_drupal_simple_oauth', $messenger,
      $network_manager, $user_authenticator, $drupal_manager,
      $request, $data_handler, $renderer);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('plugin.network.manager'),
      $container->get('social_auth.user_authenticator'),
      $container->get('social_auth_drupal_simple_oauth.manager'),
      $container->get('request_stack'),
      $container->get('social_auth.data_handler'),
      $container->get('renderer')
    );
  }

  /**
   * get datahander.
   *
   * @return \Drupal\social_auth\SocialAuthDataHandler
   */
  public function getDataHander() {
    return $this->dataHandler;
  }

  /**
   * Response for path 'user/login/drupal/callback'.
   *
   * Drupal returns the user here after user has authenticated in Drupal.
   */
  public function callback() {
    /* @var \League\OAuth2\Client\Provider\Drupal false $drupal */
    $drupal = $this->networkManager->createInstance('social_auth_drupal_simple_oauth')->getSdk();

    // If Drupal client could not be obtained.
    if (!$drupal) {
      \Drupal::messenger()->addError($this->t('Social Auth Drupal not configured properly. Contact site administrator.'));
      return $this->redirect('user.login');
    }

    $profile = $this->processCallback();

    \Drupal::moduleHandler()->invokeAll('social_auth_simple_oauth', ['data' => $profile, 'sdk' => $this->providerManager, 'controller' => $this]);

    return $this->redirect('user.login');
  }
}

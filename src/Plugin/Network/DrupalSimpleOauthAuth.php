<?php

namespace Drupal\social_auth_drupal_simple_oauth\Plugin\Network;

use Drupal\social_api\Plugin\NetworkBase;
use Drupal\social_api\SocialApiException;
use Drupal\social_auth_drupal_simple_oauth\DrupalSimpleOauth;
use Drupal\social_auth_drupal_simple_oauth\Plugin\Network\DrupalAuthInterface;
use Drupal\social_auth_drupal_simple_oauth\Settings\DrupalAuthSettings;

/**
 * Defines a Network Plugin for Social Auth Drupal.
 *
 * @package Drupal\simple_drupal_connect\Plugin\Network
 *
 * @Network(
 *   id = "social_auth_drupal_simple_oauth",
 *   social_network = "Drupal",
 *   type = "social_auth",
 *   handlers = {
 *     "settings": {
 *       "class": "\Drupal\social_auth_drupal_simple_oauth\Settings\DrupalAuthSettings",
 *       "config_id": "social_auth_drupal_simple_oauth.settings"
 *     }
 *   }
 * )
 */
class DrupalSimpleOauthAuth extends NetworkBase implements DrupalSimpleOauthAuthInterface {

  /**
   * Sets the underlying SDK library.
   *
   * @return \League\OAuth2\Client\Provider\Drupal
   *   The initialized 3rd party library instance.
   *
   * @throws SocialApiException
   *   If the SDK library does not exist.
   */
  protected function initSdk() {
    /* @var \Drupal\social_auth_drupal\Settings\DrupalAuthSettings $settings */
    $settings = $this->settings;
    $container = \Drupal::getContainer();
    $requestContentxt = $container->get('router.request_context');
    // Proxy configuration data for outward proxy.
    $proxyUrl = $this->siteSettings->get("http_client_config")["proxy"]["http"];
    if ($this->validateConfig($settings)) {
      // All these settings are mandatory.
      if ($proxyUrl) {
        $league_settings = [
          'clientId' => $settings->getClientId(),
          'clientSecret' => $settings->getClientSecret(),
          'redirectUri' => $requestContentxt->getCompleteBaseUrl() . '/user/login/simple_oauth/callback',
          'baseUrl'     => $settings->getBaseUrl(),
          'proxy' => $proxyUrl,
        ];
      }
      else {
        $league_settings = [
          'clientId' => $settings->getClientId(),
          'clientSecret' => $settings->getClientSecret(),
          'baseUrl'     => $settings->getBaseUrl(),
          'redirectUri' => $requestContentxt->getCompleteBaseUrl() . '/user/login/simple_oauth/callback',
        ];
      }

      return new DrupalSimpleOauth($league_settings);
    }
    return FALSE;
  }

  /**
   * Checks that module is configured.
   *
   * @param \Drupal\social_auth_drupal\Settings\DrupalAuthSettings $settings
   *   The Drupal auth settings.
   *
   * @return bool
   *   True if module is configured.
   *   False otherwise.
   */
  protected function validateConfig(DrupalAuthSettings $settings) {
    $client_id = $settings->getClientId();
    $client_secret = $settings->getClientSecret();
    $base_url =   $settings->getBaseUrl();
    if (!$client_id || !$client_secret || !$base_url) {
      $this->loggerFactory
        ->get('social_auth_drupal')
        ->error('Define Client ID, Client Secret and Base Url on module settings.');
      return FALSE;
    }

    return TRUE;
  }

}

<?php

namespace Drupal\social_auth_drupal_simple_oauth\Plugin\Network;

use Drupal\social_api\Plugin\NetworkInterface;

/**
 * Defines the Drupal Auth interface.
 */
interface DrupalSimpleOauthAuthInterface extends NetworkInterface {}

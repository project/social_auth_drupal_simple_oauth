<?php

namespace Drupal\social_auth_drupal_simple_oauth;

use Drupal\social_auth\AuthManager\OAuth2Manager;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Contains all the logic for Drupal login integration.
 */
class DrupalAuthManager extends OAuth2Manager {
  /**
   * Constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Used for logging errors.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Used for dispatching events to other modules.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Used for accessing Drupal user picture preferences.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   Used for generating absoulute URLs.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Used for accessing configuration object factory.
   */
  public function __construct(ConfigFactory $configFactory,
                              LoggerChannelFactoryInterface $logger_factory,
                              RequestStack $request_stack) {

    parent::__construct($configFactory->get('social_auth_drupal.settings'),
      $logger_factory,
      $request_stack->getCurrentRequest());
  }

  /**
   * Authenticates the users by using the access token.
   *
   * @return $this
   *   The current object.
   */
  public function authenticate() {
    $this->setAccessToken($this->client->getAccessToken('authorization_code',
      ['code' => $this->request->query->get('code')]));
  }

  /**
   * Gets the data by using the access token returned.
   *
   * @return League\OAuth2\Client\Provider\DrupalUser
   *   User info returned by the Drupal.
   */
  public function getUserInfo() {
    $this->user = $this->client->getResourceOwner($this->getAccessToken());
    return $this->user;
  }

  /**
   * Gets the data by using the access token returned.
   *
   * @return string
   *   Data returned by Making API Call.
   */
  public function getExtraDetails($method = 'GET', $domain = NULL) {
    if($domain) {
      $httpRequest = $this->client->getAuthenticatedRequest($method, $domain, $this->token, []);
      $data = $this->client->getResponse($httpRequest);
      return json_decode($data->getBody(), true);
    }
  }

  /**
   * Returns the Drupal login URL where user will be redirected.
   *
   * @return string
   *   Absolute Drupal login URL where user will be redirected
   */
  public function getDrupalLoginUrl() {
    $scopes = [];

    $options = ['scope' => $scopes,];
    $login_url = $this->client->getAuthorizationUrl($options);

    // Generate and return the URL where we should redirect the user.
    return $login_url;
  }

  /**
   * Returns the Drupal login URL where user will be redirected.
   *
   * @return string
   *   Absolute Drupal login URL where user will be redirected
   */
  public function getState() {
    $state = $this->client->getState();

    // Generate and return the URL where we should redirect the user.
    return $state;
  }


  /**
   * Request and end point.
   *
   * @param string      $method
   *   The HTTP method for the request.
   * @param string      $path
   *   The path to request.
   * @param string|null $domain
   *   The domain to request.
   * @param array       $options
   *   Request options.
   *
   * @return array|mixed
   *   Data returned by provider.
   */
  public function requestEndPoint($method, $path, $domain = NULL, array $options = []) {
    // TODO: Implement requestEndPoint() method.
  }

  /**
   * Returns the authorization URL where user will be redirected.
   *
   * @return string|mixed
   *   Absolute authorization URL.
   */
  public function getAuthorizationUrl() {
    return $this->client->getAuthorizationUrl();
  }
}
